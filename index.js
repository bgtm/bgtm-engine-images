var plumber = require('gulp-plumber');
var deepExtend = require('deep-extend');
var imageMin = require('gulp-imagemin');

/**
 * This the default engine for building SASS into a consolidated build
 * for many projects.
 *
 * This ensures consistent build practices across many developers within a team.
 *
 * @param taskManager
 * @param args
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        src: '',
        dest: ''
    };

    var engineOptions = deepExtend({}, defaults, args);

    if (taskManager.isEnvironment(taskManager.environment.PRODUCTION)) {
        return this.src(engineOptions.src)
            .pipe(plumber())
            .pipe(imageMin([
                // imageMin.svgo({
                //     plugins: [
                //         {cleanupIDs: false}
                //     ]
                // })
            ],{
                verbose: true
            }))
            .pipe(this.dest(engineOptions.dest))
            .pipe(done());
    }
    else {
        return this.src(engineOptions.src)
            .pipe(plumber())
            .pipe(this.dest(engineOptions.dest))
            .pipe(done());
    }
};