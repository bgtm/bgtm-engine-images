# BGTM - Engine for Image Processing

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of processing a source directory of images to its
destination.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-images

In your gulpfile add the following dependency:

    var images = require('bgtm-engine-images');

Then add the following task.

    tm.add('images', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/images/**/*'
        ],
        liveReload: true,
        engine: images,
        engineOptions: {
            'src': 'src/images/**/*',
            'dest': 'dest/images/'
        }
    });

# What it does

* Gets the source files specified in the `src` property.
* `only in production mode`: Runs the [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin)
  to minify the images in the destination folder.
* Outputs them to the destination specified in the `dest` property.

Note: The image minification process is only done when BGTM is run in
production mode.

You can do this with the following argument `gulp [optional task] --production`

# Engine Options

## Defaults

    {
        src: '',
        dest: ''
    }

| Option       | Mandatory | Description |
|--------------|-----------|-------------|
| src          | yes       | The source path to look for changes, This should be a string which can be pattern. |
| dest         | yes       | The destination file for the output to be placed into. |

# License

Copyright 2017 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.